<?php
/*
 * @package    pm_xpay
 * @version    __DEPLOY_VERSION__
 * @author     Artem Pavluk - https://nevigen.com
 * @copyright  Copyright © Nevigen.com. All rights reserved.
 * @license    Proprietary. Copyrighted Commercial Software
 * @link       https://nevigen.com
 */

defined('_JEXEC') or die;

$db = \Joomla\CMS\Factory::getDbo();

$query = $db->getQuery(true)
    ->select(array('*'))
    ->from('#__jshopping_payment_method');
$payments = $db->setQuery($query)->loadAssocList('scriptname');

if (!isset($payments['pm_xpay'])) {
    $obj = new stdClass();
    $obj->payment_code = 'xpay';
    $obj->payment_class = 'pm_xpay';
    $obj->scriptname = 'pm_xpay';
    $obj->payment_publish = 0;
    $obj->payment_type = 2;
    $obj->payment_ordering = 0;

    if (!empty($payments)) {
        foreach ($payments as $key => $payment) {
            foreach ($payment as $name => $item) {
                if (stristr($name, 'name_') !== false) {
                    $obj->$name = 'XPAY';
                }
            }
            break;
        }
    } else {
        $language = \Joomla\CMS\Component\ComponentHelper::getParams('com_languages');
        $code = $language->get('administrator', 'en-GB');
        $obj->$code = 'XPAY';
    }

    $db->insertObject('#__jshopping_payment_method', $obj);
}