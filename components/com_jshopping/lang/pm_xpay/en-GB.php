<?php
/*
 * @package    pm_xpay
 * @version    __DEPLOY_VERSION__
 * @author     Artem Pavluk - https://nevigen.com
 * @copyright  Copyright © Nevigen.com. All rights reserved.
 * @license    Proprietary. Copyrighted Commercial Software
 * @link       https://nevigen.com
 */

defined('_JEXEC') or die;

define('_JSHOP_XPAY_TOKEN','Token (PID):');
define('_JSHOP_XPAY_ACC','Client ID:');
define('_JSHOP_XPAY_ACC_DESC','Client identification method');
define('_JSHOP_XPAY_ACC_PHONE','Phone number');
define('_JSHOP_XPAY_ACC_EMAIL','Email');
define('_JSHOP_XPAY_PAYMENT_INFO','PaymentInfo Content:');
define('_JSHOP_XPAY_PAYMENT_INFO_ORDER','Order number');
define('_JSHOP_XPAY_PAYMENT_INFO_PRODUCTS','List of products');
define('_JSHOP_XPAY_TRANSACTION_STATUS_PENDING','Order status for pending transactions:');
define('_JSHOP_XPAY_TRANSACTION_STATUS_SUCCESS','Order status for successful transactions:');
define('_JSHOP_XPAY_TRANSACTION_STATUS_FAILED','Order status for failed transactions:');

define('_JSHOP_XPAY_SHIPPING_TITLE','Shipping');

define('_JSHOP_XPAY_ERROR_OPENSSL_VERIFY','Openssl_verify function not found');