<?php
/*
 * @package    pm_xpay
 * @version    __DEPLOY_VERSION__
 * @author     Artem Pavluk - https://nevigen.com
 * @copyright  Copyright © Nevigen.com. All rights reserved.
 * @license    Proprietary. Copyrighted Commercial Software
 * @link       https://nevigen.com
 */

defined('_JEXEC') or die;

define('_JSHOP_XPAY_TOKEN','Токен (PID):');
define('_JSHOP_XPAY_ACC','Идентификатор клиента:');
define('_JSHOP_XPAY_ACC_DESC','Способ идентификации клиента');
define('_JSHOP_XPAY_ACC_PHONE','Номер телефона');
define('_JSHOP_XPAY_ACC_EMAIL','Email');
define('_JSHOP_XPAY_PAYMENT_INFO','Содержимое PaymentInfo:');
define('_JSHOP_XPAY_PAYMENT_INFO_ORDER','Номер заказа');
define('_JSHOP_XPAY_PAYMENT_INFO_PRODUCTS','Список товаров');
define('_JSHOP_XPAY_TRANSACTION_STATUS_PENDING','Статус заказа для незавершенных транзакций:');
define('_JSHOP_XPAY_TRANSACTION_STATUS_SUCCESS','Статус заказа для успешных транзакций:');
define('_JSHOP_XPAY_TRANSACTION_STATUS_FAILED','Статус заказа для неуспешных транзакций:');

define('_JSHOP_XPAY_SHIPPING_TITLE','Доставка');

define('_JSHOP_XPAY_ERROR_OPENSSL_VERIFY','Не найдена функция openssl_verify');