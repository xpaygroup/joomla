<?php
/*
 * @package    pm_xpay
 * @version    __DEPLOY_VERSION__
 * @author     Artem Pavluk - https://nevigen.com
 * @copyright  Copyright © Nevigen.com. All rights reserved.
 * @license    Proprietary. Copyrighted Commercial Software
 * @link       https://nevigen.com
 */

defined('_JEXEC') or die;

define('_JSHOP_XPAY_TOKEN','Токен (PID):');
define('_JSHOP_XPAY_ACC','Ідентифікатор клієнта:');
define('_JSHOP_XPAY_ACC_DESC','Спосіб ідентифікації клієнта');
define('_JSHOP_XPAY_ACC_PHONE','Номер телефону');
define('_JSHOP_XPAY_ACC_EMAIL','Email');
define('_JSHOP_XPAY_PAYMENT_INFO','Вміст PaymentInfo:');
define('_JSHOP_XPAY_PAYMENT_INFO_ORDER','Номер замовлення');
define('_JSHOP_XPAY_PAYMENT_INFO_PRODUCTS','Список товарів');
define('_JSHOP_XPAY_TRANSACTION_STATUS_PENDING','Статус замовлення для незавершених транзакцій:');
define('_JSHOP_XPAY_TRANSACTION_STATUS_SUCCESS','Статус замовлення для успішних транзакцій:');
define('_JSHOP_XPAY_TRANSACTION_STATUS_FAILED','Статус замовлення для неуспішних транзакцій:');

define('_JSHOP_XPAY_SHIPPING_TITLE','Доставка');

define('_JSHOP_XPAY_ERROR_OPENSSL_VERIFY','Не знайдено функцію openssl_verify');
