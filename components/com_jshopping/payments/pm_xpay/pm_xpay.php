<?php
/*
 * @package    pm_xpay
 * @version    __DEPLOY_VERSION__
 * @author     Artem Pavluk - https://nevigen.com
 * @copyright  Copyright © Nevigen.com. All rights reserved.
 * @license    Proprietary. Copyrighted Commercial Software
 * @link       https://nevigen.com
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Installer\Installer;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;

class pm_xpay extends PaymentRoot
{
	protected $public_key = array(
		'test' => "-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0IH2V0Ot1ej4FdOihujG
ON37sqql62vFFR/4IK+w4xqHRvD+SEwwkLL9EO72e42bV9VaKOqKbX81A+0hbBXi
W7axjHU2Sc97EXTHjpwX++HduUXbXhRteyzcHDLZCGKT8WzoNgQeXcieLUYUp2bb
gjElGecKprcprkMeHmffmelwlzcv61auGU0o10CTyyCqhOKofdqJq6A2KOBCLL49
5z1700oCRo9qL4loe95r4wGh6AmHZNvAnAwLgzwzyLvWCz479CVIWEaMY/+uczfL
0yRjN+8uqNK3A09wOD+wO1I+YfU9YXcQ75L8ibxzWcNgMHrhJQ9ZtnoVltiTWEEB
9QIDAQAB
-----END PUBLIC KEY-----",
		'prod' => "-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9+1AEFfD9MoO0IWeMk3f
aFoYBBekFgHmUGM48AVh6BW/s5r16mtUfMfRfezVgqluwV/liEd6hArmmEZIKwYE
mJoAYuY/ny9QJpc8zY+toR5IJEtYxfStHmVwKSuvHL3KY/U/Ok5UUT2u075JPZb+
FtDZwW9KXkwmT53HQ6iS0XFyy621vGrs6XcdGwO6eZPptkvc8SYKDwClgLjI69Iz
b6K/dfdQUioMPvZOXpdzrEQXjnipmsYh1VxOufqsX1SDzqR67Zs114OnHWAZhTXE
ksUjKavJkCc07T+nu1O/r99rsrRCaQODVq8SMAoK1vxJLf29WFv4ydp4vIk+n98/
DQIDAQAB
-----END PUBLIC KEY-----"
	);

	public function __construct()
	{
		JSFactory::loadExtLanguageFile('pm_xpay');
	}

	public function showAdminFormParams($params)
	{
		if (!is_array($params)) $params = array();
		$array_params = array('testmode', 'pid', 'acc', 'payment_info', 'transaction_pending_status', 'transaction_end_status', 'transaction_failed_status');
		foreach ($array_params as $key)
		{
			if (!isset($params[$key])) $params[$key] = '';
		}

		if (!function_exists('openssl_verify'))
		{
			Factory::getApplication()->enqueueMessage(_JSHOP_XPAY_ERROR_OPENSSL_VERIFY, 'error');

			return false;
		}

		$acc          = array(
			array(
				'name'  => _JSHOP_XPAY_ACC_EMAIL,
				'value' => 'email'
			),
			array(
				'name'  => _JSHOP_XPAY_ACC_PHONE,
				'value' => 'phone'
			),
		);
		$payment_info = array(
			array(
				'name'  => _JSHOP_XPAY_PAYMENT_INFO_ORDER,
				'value' => 'order'
			),
			array(
				'name'  => _JSHOP_XPAY_PAYMENT_INFO_PRODUCTS,
				'value' => 'products'
			),
		);
		$orders       = JSFactory::getModel('orders');
		$config      = JSFactory::getConfig();
		$xmlData     = Installer::parseXMLInstallFile($config->admin_path . 'jshopping.xml');
		if ((int) $xmlData['version'] === 5)
		{
			$newVersion = true;
		}
		else{
			$newVersion = false;
		}

		include_once(__DIR__ . '/adminparamsform.php');
	}

	public function showPaymentForm($params, $pmconfigs){
		$app = Factory::getApplication();
		if (empty($app->input->cookie->get('fingerprint')))
		{
			HTMLHelper::_('script', 'https://cdn.jsdelivr.net/npm/@fingerprintjs/fingerprintjs@3/dist/fp.min.js', ['version' => 'auto', 'async' => 'async']);

			echo '<script>
		FingerprintJS.load().then(function (fp) {
            fp.get().then(function (result) {
               let now = new Date();
               now.setTime(now.getTime() + (365* 24 * 60 * 60 * 1000));
              let cookievalue = result.visitorId + ";"
             
               document.cookie="fingerprint=" + cookievalue;
               document.cookie = "expires=" + now.toUTCString() + ";path=/"
            })
        });
		</script>';
		}
	}
	public function showEndForm($pmconfigs, $order)
	{
		$app = Factory::getApplication();
		if (!empty($fingerprint = $app->input->cookie->get('fingerprint')))
		{
			$url = $this->getPaymentUrl($pmconfigs, $order, $fingerprint);
			if ($url)
			{
				echo '<script>location.href = "' . $url . '"</script>';
			}
		}

	}

	public function checkTransaction($pmconfigs, $order, $act)
	{
		if ($act != 'notify')
		{
			return;
		}

		$input   = Factory::getApplication()->input;
		$command = $input->get('command', null);
		if (empty($command))
		{
			$this->displayResponse(
				['result'    => "21",
				 'txn_id'    => $input->get('txn_id'),
				 'message'   => 'Command not found!',
				 'date_time' => date('YmdHis')
				]
			);
		}

		if ($this->validateResponse($pmconfigs) == 0)
		{
			$this->displayResponse(
				['result'    => "21",
				 'txn_id'    => $input->get('txn_id'),
				 'message'   => 'Signature is not valid!',
				 'date_time' => date('YmdHis')
				]
			);
		}

		if ((int) $order->order_status !== (int) $pmconfigs['transaction_end_status']
			&& (int) $order->order_status !== (int) $pmconfigs['transaction_failed_status'])
		{
			if (!empty($command))
			{
				if ($command === 'pay')
				{
					return array(1, 'Status Success. Order ID ' . $order->order_id);
				}
				elseif ($command === 'error')
				{
					return array(3, 'Status Failed. Order ID ' . $order->order_id);
				}
			}
		}
		else
		{
			$this->displayResponse([
				'result'    => "10",
				'txn_id'    => $input->get('txn_id'),
				'message'   => 'Ok',
				'date_time' => date('YmdHis'),
			]);
		}
	}

	public function getUrlParams($pmconfigs)
	{
		$params = array();

		$params['order_id']          = Factory::getApplication()->input->get('txn_id');
		$params['checkReturnParams'] = 0;
		$params['hash']              = '';
		$params['checkHash']         = 0;

		return $params;
	}

	protected function getPaymentUrl($params, $order, $fingerprint)
	{
		if (empty($params['pid']))
		{
			Factory::getApplication()->enqueueMessage('Error params pid', 'error');

			return false;
		}
		$config      = JSFactory::getConfig();
		$xmlData     = Installer::parseXMLInstallFile($config->admin_path . 'jshopping.xml');
		$uri         = Uri::getInstance();
		$liveurlhost = $uri->toString(array('scheme', 'host', 'port'));
		$pm_method   = $this->getPmMethod();
		$url         = 'https://mapi.xpay.com.ua';
		if (isset($params['testmode']) && (int) $params['testmode'] === 1)
		{
			$url = 'https://stage-mapi.xpay.com.ua';
		}
		$url      .= '/uk/frame/widget/banner-payment?pid=' . $params['pid'];
		$phone    = false;
		$products = [];
		$total    = (float) $order->order_total * 100;
		if (!empty($order->phone))
		{
			$phone = $order->phone;
		}
		elseif (!empty($order->mobil_phone))
		{
			$phone = $order->mobil_phone;
		}

		if (!empty($phone))
		{
			$phone = $this->clearPhone($phone);
		}
		$acc = $order->email;
		if (!empty($params['acc']) && $params['acc'] === 'phone' && !empty($phone))
		{
			$acc = $phone;
		}

		if ((int) $xmlData['version'] === 5)
		{
			$return   = $liveurlhost . JSHelper::SEFLink('index.php?option=com_jshopping&controller=checkout&task=step7&act=return&js_paymentclass=' . $pm_method->payment_class . '&order_id=' . $order->order_id);
			$callback = $liveurlhost . JSHelper::SEFLink('index.php?option=com_jshopping&controller=checkout&task=step7&act=notify&js_paymentclass=' . $pm_method->payment_class . '&no_lang=1');
		}
		else
		{
			$return   = $liveurlhost . SEFLink('index.php?option=com_jshopping&controller=checkout&task=step7&act=return&js_paymentclass=' . $pm_method->payment_class . '&order_id=' . $order->order_id);
			$callback = $liveurlhost . SEFLink('index.php?option=com_jshopping&controller=checkout&task=step7&act=notify&js_paymentclass=' . $pm_method->payment_class . '&no_lang=1&order_id=' . $order->order_id);

		}

		$payment_data = array(
			'Email'       => $order->email,
			'txn_id'      => (string) $order->order_id,
			'CallBackURL' => $callback,
			'Callback'    => [
				'PaySuccess' => [
					'URL' => $return
				]
			]
		);
		if (!empty($order->f_name))
		{
			$payment_data['FirstName'] = $order->f_name;
		}
		if (!empty($order->l_name))
		{
			$payment_data['LastName'] = $order->l_name;
		}
		if (!empty($order->currency_code_iso))
		{
			$payment_data['Currency'] = $order->currency_code_iso;
		}
		if (!empty($fingerprint))
		{
			$payment_data['BrowserFingerprint'] = $fingerprint;
		}
		if (!empty($phone))
		{
			$payment_data['Phone'] = $phone;
		}

		if (!empty($order->ip_address))
		{
			$payment_data['ClientIP'] = $order->ip_address;
		}
		if (!empty($params['payment_info']) && $params['payment_info'] === 'products')
		{
			foreach ($order->getAllItems() as $item)
			{
				$sum     = (float) $item->product_item_price * (int) $item->product_quantity;
				$product = array(
					'Caption' => $item->product_name,
					'Value'   => $sum * 100,
				);

				$product['sum'] = $sum * 100;

				$products[] = $product;
			}

			if (!empty($order->order_shipping))
			{
				$sum     = (float) $order->order_shipping;
				$product = array(
					'Caption' => _JSHOP_XPAY_SHIPPING_TITLE,
					'Value'   => $sum * 100,
				);

				$products[] = $product;
			}
		}
		else
		{
			if ((int) $xmlData['version'] === 5)
			{
				$item_name = Text::sprintf('JSHOP_PAYMENT_NUMBER', $order->order_number);
			}
			else
			{
				$item_name = sprintf(_JSHOP_PAYMENT_NUMBER, $order->order_number);
			}
			$products = array(
				'Caption' => $item_name,
				'Value'   => '#' . $order->order_number
			);

		}
		if (!empty($products))
		{
			$payment_data['PaymentInfo'] = $products;
		}

		$data   = urlencode(base64_encode(gzencode(json_encode($payment_data))));
		$result = $url . '&acc=' . $acc . '&sum=' . $total . '&data=' . $data;

		return $result;
	}

	protected function clearPhone($phoneSource)
	{
		if (empty($phoneSource)) return false;
		$phone = preg_replace('/\D/', '', $phoneSource);
		if (strlen($phone) == 10)
		{
			$phone = '38' . $phone;
		}
		elseif (strlen($phone) == 11)
		{
			$phone = '3' . $phone;
		}

		return $phone;
	}

	protected function validateResponse($params)
	{
		$input = Factory::getApplication()->input;

		$key = $this->public_key['prod'];
		if (isset($params['testmode']) && (int) $params['testmode'] === 1)
		{
			$key = $this->public_key['test'];
		}
		$string_data = $input->getString('txn_id')
			. $input->getString('uuid')
			. $input->getString('txn_date')
			. $input->getString('sum');


		$signature = $input->getString('sign', '');
		$signature = urlencode($signature);
		$signature = str_replace(array('%2B','%3D','%2F'), array('+','=','/'),$signature);

		return openssl_verify($string_data, base64_decode($signature),
			$key, OPENSSL_ALGO_SHA256);

	}

	protected function displayResponse($data = array())
	{
		header('Content-Type: application/json');
		echo json_encode($data);
		exit();
	}

	protected function saveDebug($type, $act = null)
	{
		$filename = JPATH_ROOT . '/' . $type . '.txt';
		if (\Joomla\CMS\Filesystem\File::exists($filename)) \Joomla\CMS\Filesystem\File::delete($filename);
		$data = array(
			'post'    => $_POST,
			'get'     => $_GET,
			'request' => $_REQUEST,
			'act'     => (!empty($act)) ? $act : ''

		);
		file_put_contents($filename, print_r($data, true));

	}


}