<?php
/*
 * @package    pm_xpay
 * @version    __DEPLOY_VERSION__
 * @author     Artem Pavluk - https://nevigen.com
 * @copyright  Copyright © Nevigen.com. All rights reserved.
 * @license    Proprietary. Copyrighted Commercial Software
 * @link       https://nevigen.com
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\Component\Jshopping\Administrator\Model\OrdersModel;
use Joomla\Component\Jshopping\Site\Helper\SelectOptions;

/**
 * Layout variables
 * -----------------
 *
 * @var  array       $params     Params admin.
 * @var  array       $acc
 * @var  array       $payment_info
 * @var  OrdersModel $orders     Model orders.
 * @var  bool        $newVersion Check Joomshopping 5.
 *
 */

?>

<div class="col100">
	<fieldset class="adminform">
		<table class="admintable">
			<tr>
				<td style="width:250px;" class="key">
					Test mode
				</td>
				<td>
					<?php
					echo HTMLHelper::_('select.booleanlist', 'pm_params[testmode]', 'class = "inputbox" size = "1"', $params['testmode']);
					?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo _JSHOP_XPAY_TOKEN; ?>
				</td>
				<td>
					<input type="text" class="inputbox form-control" name="pm_params[pid]" size="45"
						   value="<?php echo $params['pid'] ?>" required/>
					<?php
					if ($newVersion) echo JSHelperAdmin::tooltip(_JSHOP_XPAY_TOKEN);
					else echo ' ' . HTMLHelper::tooltip(_JSHOP_XPAY_TOKEN);
					?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo _JSHOP_XPAY_ACC ?>
				</td>
				<td>
					<?php
					echo HTMLHelper::_('select.genericlist', $acc, 'pm_params[acc]',
						'class = "inputbox custom-select" size = "1" style="max-width:240px; display: inline-block"',
						'value', 'name', $params['acc']);
					if ($newVersion) echo JSHelperAdmin::tooltip(_JSHOP_XPAY_ACC);
					else echo ' ' . HTMLHelper::tooltip(_JSHOP_XPAY_ACC);
					?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo _JSHOP_XPAY_PAYMENT_INFO ?>
				</td>
				<td>
					<?php
					echo HTMLHelper::_('select.genericlist', $payment_info, 'pm_params[payment_info]',
						'class = "inputbox custom-select" size = "1" style="max-width:240px; display: inline-block"',
						'value', 'name', $params['acc']);
					if ($newVersion) echo JSHelperAdmin::tooltip(_JSHOP_XPAY_PAYMENT_INFO);
					else echo ' ' . HTMLHelper::tooltip(_JSHOP_XPAY_PAYMENT_INFO);
					?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo _JSHOP_XPAY_TRANSACTION_STATUS_PENDING ?>
				</td>
				<td>
					<?php
					echo HTMLHelper::_('select.genericlist', $orders->getAllOrderStatus(), 'pm_params[transaction_pending_status]', 'class = "inputbox custom-select" size = "1" style="max-width:240px; display: inline-block"', 'status_id', 'name', $params['transaction_pending_status']);
					if ($newVersion) echo JSHelperAdmin::tooltip(_JSHOP_XPAY_TRANSACTION_STATUS_PENDING);
					else echo ' ' . HTMLHelper::tooltip(_JSHOP_XPAY_TRANSACTION_STATUS_PENDING);
					?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo _JSHOP_XPAY_TRANSACTION_STATUS_SUCCESS ?>
				</td>
				<td>
					<?php
					if (empty($params['transaction_end_status'])) $params['transaction_end_status'] = 6;
					echo HTMLHelper::_('select.genericlist', $orders->getAllOrderStatus(), 'pm_params[transaction_end_status]', 'class = "inputbox custom-select" size = "1" style="max-width:240px; display: inline-block"', 'status_id', 'name', $params['transaction_end_status']);
					if ($newVersion) echo JSHelperAdmin::tooltip(_JSHOP_XPAY_TRANSACTION_STATUS_SUCCESS);
					else echo ' ' . HTMLHelper::tooltip(_JSHOP_XPAY_TRANSACTION_STATUS_SUCCESS);
					?>
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo _JSHOP_XPAY_TRANSACTION_STATUS_FAILED ?>
				</td>
				<td>
					<?php
					if (empty($params['transaction_failed_status'])) $params['transaction_failed_status'] = 3;
					echo HTMLHelper::_('select.genericlist', $orders->getAllOrderStatus(), 'pm_params[transaction_failed_status]', 'class = "inputbox custom-select" size = "1" style="max-width:240px; display: inline-block"', 'status_id', 'name', $params['transaction_failed_status']);
					if ($newVersion) echo JSHelperAdmin::tooltip(_JSHOP_XPAY_TRANSACTION_STATUS_FAILED);
					else echo ' ' . HTMLHelper::tooltip(_JSHOP_XPAY_TRANSACTION_STATUS_FAILED);
					?>
				</td>
			</tr>
		</table>
	</fieldset>
</div>
